package com.example.demo.service;

import com.example.demo.models.databse.Users;
import com.example.demo.reponsitory.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    private UsersRepository usersRepository;

    @Override
    public UserDetails loadUserByUsername(String usesrname) {
        // Kiểm tra xem user có tồn tại trong database không?
        Users users = usersRepository.findByUsername(usesrname);
        if (users == null) {
            throw new UsernameNotFoundException(usesrname);
        }
        return new CustomUserDetails(users);
    }


    public UserDetails loadUserById(Long userId) {
        Users users = usersRepository.findByUserId(userId);
        if (users == null) {
            throw new UsernameNotFoundException(String.valueOf(userId));
        }
        return new CustomUserDetails(users);
    }
}