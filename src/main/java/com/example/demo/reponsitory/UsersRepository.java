package com.example.demo.reponsitory;

import com.example.demo.models.databse.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface UsersRepository extends JpaRepository<Users, Long>, JpaSpecificationExecutor<Users> {
    Users findByUsername(String username);
    Users findByUserId(Long user_id);
}