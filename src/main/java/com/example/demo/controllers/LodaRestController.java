package com.example.demo.controllers;


import com.example.demo.models.rest.LoginRequest;
import com.example.demo.security.JwtTokenProvider;
import com.example.demo.service.CustomUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;

@RestController
@RequestMapping("/api")
public class LodaRestController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenProvider tokenProvider;

    @Autowired
    public PasswordEncoder passwordEncoder;



    @PostMapping("/login")
    public String authenticateUser( @RequestBody LoginRequest loginRequest ) {
        // Xác thực từ username và password.
        Authentication authentication = null;
        String jwt = null;
        try {
             authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            loginRequest.getUsername(),loginRequest.getPassword()
                    )
            );

            // Nếu không xảy ra exception tức là thông tin hợp lệ
            // Set thông tin authentication vào Security Context
            SecurityContextHolder.getContext().setAuthentication(authentication);

            // Trả về jwt cho người dùng.
             jwt = tokenProvider.generateToken((CustomUserDetails) authentication.getPrincipal());
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        return jwt;
    }

    // Api /api/random yêu cầu phải xác thực mới có thể request
   // @PreAuthorize()
   // @RolesAllowed("ADMIN")
    @GetMapping("/ping")
    public String randomStuff(){
        return passwordEncoder.encode("admin");
    }

}